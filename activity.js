db.users.find(
    {
        $or : [
            { first_name: { $regex: 's', $options: '$i' } },
            { last_name: { $regex: 'd', $options: '$i' } }
        ]
    },
    { first_name: 1, last_name: 1, _id: 0}
);

db.users.find(
    {
        $and : [
            { department: "HR" },
            { age: { $gte: 70 } }
        ]
    }
);

db.users.find(
    {
        $and : [
            { first_name: { $regex: 'e', $options: '$i' } },
            { age: { $lte: 30 } }
        ]
    }
);